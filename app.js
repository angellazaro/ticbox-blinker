console.log('Starting process');
//var rpiIOEndPoint = 'http://127.0.0.1:8002';
var rpiIOEndPoint = 'http://rpi-io:8002';
const request = require('request');

var interval = 1000;
var timeOut = 15000;

var control = 0;

var pin1 = 4;
var pin2 = 17;
var pin3 = 27;
var pin4 = 22;
var pin5 = 18;

var blinker = setInterval(blinkLED, interval);

function blinkLED() {
	
	const options = { 
		url: rpiIOEndPoint + '/api/do/' + pin1,
		method: 'GET',
		headers: {
			'Accept': 'application/json',
			'Accept-Charset': 'utf-8',
			'User-Agent': 'rpi-blinker'
		}
	};
	
	var value = request(options, function(err, res, body) {
		var r = JSON.parse(body);
		console.log("rpi-blinker-read (err) => " + err);
		console.log("rpi-blinker-read (res) => " + res);
		console.log("rpi-blinker-read (body) => " + body);
		console.log("rpi-blinker-read (r.response.value) => " + r.response.value);
		
		if (control === 0) {
			write(pin2, 1);
			write(pin4, 1);
			
			write(pin1, 0);
			write(pin3, 0);
			write(pin5, 0);
			
			control = 1;
		} else {
			write(pin2, 0);
			write(pin4, 0);
			
			write(pin1, 1);
			write(pin3, 1);
			write(pin5, 1);
			
			control = 0;
		}
	});	
}


function write(pin, value){

	console.log("rpi-blinker-write (pin) => " + pin);
	console.log("rpi-blinker-write (value) => " + value);

	const options = { 
		url: rpiIOEndPoint + '/api/di/' + pin,
		method: 'PATCH',
		headers: {
			'Accept': 'application/json',
			'Accept-Charset': 'utf-8',
			'User-Agent': 'rpi-blinker'
		},
		json: {
			'value': value
		}
	};
	
	request(options, function(err, res, body) {
		console.log("rpi-blinker-write (err) => " + err);
		console.log("rpi-blinker-write (res) => " + res);
		console.log("rpi-blinker-write (body) => " + body);
	});
}

function endBlink() {
	clearInterval(blinker);
	write(pin2, 0);
	write(pin4, 0);	
	write(pin1, 0);
	write(pin3, 0);
	write(pin5, 0);
	
	control = 0;
}

//setTimeout(endBlink, timeOut);