# Descarga

La descarga de la imagen se podrá realizar haciendo uso del siguente comando:

```
docker pull angellazaro/ticbox-blinker
```

# Arrancar contenedor

```
docker run -d --net rpi-net --name blinker angellazaro/ticbox-blinker
```

# Prerrequisitos

Instalación y arranque de la capa de abstracción https://gitlab.com/angellazaro/ticbox-rpi-io